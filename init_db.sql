use book;

CREATE TABLE IF NOT EXISTS users (
  id INT(10) AUTO_INCREMENT,
  name VARCHAR(255),
  password VARCHAR(255) NOT NULL,
  username VARCHAR(255) NOT NULL,
  surname VARCHAR(255),
  date_of_birth VARCHAR(10) NOT NULL,
  created_date datetime NOT NULL,
PRIMARY KEY ( id )
);

CREATE INDEX users_idx01
ON users (username);

CREATE TABLE IF NOT EXISTS books (
  id INT(10) AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  author VARCHAR(255),
  price VARCHAR(255),
  is_recommended BOOLEAN NOT NULL,
  created_date datetime NOT NULL,
PRIMARY KEY ( id ),
CONSTRAINT books_uniq01 UNIQUE (name)
);

CREATE INDEX books_idx01
ON books (name,is_recommended);

CREATE TABLE IF NOT EXISTS orders (
  id INT(10) AUTO_INCREMENT,
  user_id INT(10) NOT NULL,
  book_id INT(10) NOT NULL,
  created_date datetime NOT NULL,
FOREIGN KEY (user_id) REFERENCES users(id),
FOREIGN KEY (book_id) REFERENCES books(id),
PRIMARY KEY ( id )
);

CREATE INDEX order_idx01
ON orders (user_id);
