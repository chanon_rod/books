package com.example.book.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class CommonException extends Exception {

    private final String code;
    private final String message;
    private final HttpStatus httpStatus;

    public CommonException(String code, String message, HttpStatus httpStatus) {
        this.code = code;
        this.message = message;
        this.httpStatus = httpStatus;
    }
}
