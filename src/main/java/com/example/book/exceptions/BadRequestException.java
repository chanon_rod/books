package com.example.book.exceptions;

import lombok.Getter;

@Getter
public class BadRequestException extends Exception {

    private final String code;
    private final String message;

    public BadRequestException(String code, String message) {
        this.code = code;
        this.message = message;
    }
}