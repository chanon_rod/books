package com.example.book.repositories;

import com.example.book.entities.UsersEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface UsersRepository extends JpaRepository<UsersEntity, Long> {
    UsersEntity findByUsername(String username);

}
