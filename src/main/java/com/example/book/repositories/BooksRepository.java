package com.example.book.repositories;

import com.example.book.entities.BooksEntity;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface BooksRepository extends JpaRepository<BooksEntity, Integer> {
    BooksEntity findById(Long id);
    List<BooksEntity> findAllByOrderByIsRecommendedDesc(Sort sort);

}
