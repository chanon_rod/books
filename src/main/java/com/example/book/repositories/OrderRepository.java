package com.example.book.repositories;

import com.example.book.entities.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import java.util.Set;

@Transactional
public interface OrderRepository extends JpaRepository<OrderEntity, Long> {
    Set<OrderEntity> findAllByUserId(Long id);

}
