package com.example.book.constants;

public enum Response {

    SUCCESS("0", "Success"),
    INVALID_REQUEST("ERROR.INVALID_REQUEST", "%s is invalid"),
    LOGIN_FAIL("ERROR.LOGIN_FAIL", "user login fail"),
    USER_NOT_FOUND("ERROR.USER_NOT_FOUND", "user not found"),
    ORDER_NOT_FOUND("ERROR.ORDER_NOT_FOUND", "order not found"),
    LOGIN_TOKEN_INVALID_OR_EXPIRED("ERROR.LOGIN_TOKEN_INVALID_OR_EXPIRED", "login token is invalid or expired"),
    INTERNAL_SERVER_ERROR("ERROR.INTERNAL_SERVER_ERROR", "internal server error");


    protected final String code;
    protected final String message;

    Response(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}