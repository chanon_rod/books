package com.example.book.constants;

public class Constant {
    public static final int EXPIRE_LOGIN_TOKEN = 900;
    public static final String LOGIN_TOKEN = "Login-Token";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String DATE_OF_BIRTH = "date_of_birth";
}
