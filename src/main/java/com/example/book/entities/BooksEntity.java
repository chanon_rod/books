package com.example.book.entities;


import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "books")
public class BooksEntity {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "author")
    private String author;

    @Column(name = "price")
    private String price;

    @Column(name = "is_recommended")
    private boolean isRecommended;

    @Column(name = "created_date")
    private Date createdDate;
}
