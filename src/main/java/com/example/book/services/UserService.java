package com.example.book.services;

import com.example.book.configs.ApplicationConfiguration;
import com.example.book.constants.Constant;
import com.example.book.constants.Response;
import com.example.book.entities.BooksEntity;
import com.example.book.entities.OrderEntity;
import com.example.book.entities.UsersEntity;
import com.example.book.exceptions.BadRequestException;
import com.example.book.exceptions.CommonException;
import com.example.book.models.*;
import com.example.book.repositories.BooksRepository;
import com.example.book.repositories.OrderRepository;
import com.example.book.repositories.UsersRepository;
import com.example.book.utils.Aes256Coder;
import com.example.book.utils.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.crypto.NoSuchPaddingException;
import java.math.BigDecimal;
import java.nio.BufferUnderflowException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.*;


@Service
public class UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    private UsersRepository usersRepository;

    private OrderRepository orderRepository;

    private BooksRepository booksRepository;

    private ApplicationConfiguration applicationConfiguration;

    private RedisService redisService;

    @Autowired
    public UserService(UsersRepository usersRepository, ApplicationConfiguration applicationConfiguration,
                       RedisService redisService, OrderRepository orderRepository, BooksRepository booksRepository) {
        this.usersRepository = usersRepository;
        this.applicationConfiguration = applicationConfiguration;
        this.redisService = redisService;
        this.orderRepository = orderRepository;
        this.booksRepository = booksRepository;
    }

    public String login(UserRequest userRequest) throws BadRequestException, CommonException {
        UsersEntity usersEntity = usersRepository.findByUsername(userRequest.getUsername());

        if (usersEntity == null) {
            logger.error("Login fail with username not found, username : [{}]", userRequest.getUsername());
            throw new BadRequestException(Response.LOGIN_FAIL.getCode(), Response.LOGIN_FAIL.getMessage());
        } else {
            try {
                String decryptPassword = Aes256Coder.decrypt(usersEntity.getPassword(), applicationConfiguration.getEncryptKey());

                if (userRequest.getPassword().equals(decryptPassword)) {
                    String loginTokenKey = Util.getUUID();
                    redisService.putRedisString(loginTokenKey, userRequest.getUsername(), Constant.EXPIRE_LOGIN_TOKEN);
                    return loginTokenKey;
                } else {
                    logger.error("Login fail with wrong password, username : [{}]", userRequest.getUsername());
                    throw new BadRequestException(Response.LOGIN_FAIL.getCode(), Response.LOGIN_FAIL.getMessage());
                }

            } catch (NoSuchPaddingException| NoSuchAlgorithmException| InvalidKeySpecException|
                    InvalidAlgorithmParameterException| InvalidKeyException | BufferUnderflowException e) {
                logger.error("Login fail with decrypt password error");
                throw new CommonException(Response.INTERNAL_SERVER_ERROR.getCode(), Response.INTERNAL_SERVER_ERROR.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    public void createUser(CreateUserRequest userRequest) throws BadRequestException, CommonException {
        UsersEntity usersData = usersRepository.findByUsername(userRequest.getUsername());

        if (usersData != null) {
            logger.error("Create user fail with username not found, username : [{}]", userRequest.getUsername());
            throw new BadRequestException(Response.USER_NOT_FOUND.getCode(), Response.USER_NOT_FOUND.getMessage());
        } else {
            try {
                String decryptPassword = Aes256Coder.encrypt(userRequest.getPassword(), applicationConfiguration.getEncryptKey());

                UsersEntity usersEntity = new UsersEntity();
                usersEntity.setPassword(decryptPassword);
                usersEntity.setUsername(userRequest.getUsername());
                usersEntity.setDateOfBirth(userRequest.getDateOfBirth());
                usersEntity.setName(userRequest.getName());
                usersEntity.setSurname(userRequest.getSurname());
                usersEntity.setCreatedDate(new Date());
                usersRepository.save(usersEntity);

            } catch (Exception e) {
                logger.error("create fail with decrypt password error");
                throw new CommonException(Response.INTERNAL_SERVER_ERROR.getCode(), Response.INTERNAL_SERVER_ERROR.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    public UserInformation getUserInfo(String loginToken) throws BadRequestException {
        Object username = redisService.getRedisStringByKeyValue(loginToken);
        if(username == null){
            logger.error("Get user info fail with login token invalid or expired");
            throw new BadRequestException(Response.LOGIN_TOKEN_INVALID_OR_EXPIRED.getCode(), Response.LOGIN_TOKEN_INVALID_OR_EXPIRED.getMessage());
        }

        UsersEntity usersData = usersRepository.findByUsername(username.toString());

        if(usersData == null){
            logger.error("Get user info fail with username not found, username : [{}]", username.toString());
            throw new BadRequestException(Response.USER_NOT_FOUND.getCode(), Response.USER_NOT_FOUND.getMessage());
        }

        Set<OrderEntity> orderEntityList =  orderRepository.findAllByUserId(usersData.getId());

        UserInformation userInformation = new UserInformation();
        userInformation.setName(usersData.getName());
        userInformation.setSurname(usersData.getSurname());
        userInformation.setDateOfBirth(usersData.getDateOfBirth());

        if(!orderEntityList.isEmpty()){
            List<Long> orderList = new ArrayList<>();
            for (OrderEntity orderEntity: orderEntityList){
                orderList.add(orderEntity.getBookId());
            }
            userInformation.setBooks(orderList);
        }

        return userInformation;

    }

    public OrderBookResponse orderBook(String loginToken, OrderBookRequest orderBookRequest) throws BadRequestException {
        Object username = redisService.getRedisStringByKeyValue(loginToken);
        if(username == null){
            logger.error("Order book fail with login token invalid or expired");
            throw new BadRequestException(Response.LOGIN_TOKEN_INVALID_OR_EXPIRED.getCode(), Response.LOGIN_TOKEN_INVALID_OR_EXPIRED.getMessage());
        }

        UsersEntity usersData = usersRepository.findByUsername(username.toString());

        if(usersData == null){
            logger.error("Order book fail with username not found, username : [{}]", username.toString());
            throw new BadRequestException(Response.USER_NOT_FOUND.getCode(), Response.USER_NOT_FOUND.getMessage());
        }
        BigDecimal bigDecimal = new BigDecimal(0).setScale(2);
        for(Long bookId: orderBookRequest.getOrders()){
            BooksEntity booksEntity = booksRepository.findById(bookId);
            if(booksEntity == null){
                logger.error("Order book fail with order not found, username : [{}]", username.toString());
                throw new BadRequestException(Response.ORDER_NOT_FOUND.getCode(), Response.ORDER_NOT_FOUND.getMessage());
            }
            bigDecimal = bigDecimal.add(new BigDecimal(booksEntity.getPrice()));
        }

        Date date = new Date();
        for(Long bookId: orderBookRequest.getOrders()){
            OrderEntity orderEntity = new OrderEntity();
            orderEntity.setUserId(usersData.getId());
            orderEntity.setBookId(bookId);
            orderEntity.setCreatedDate(date);
            orderRepository.save(orderEntity);
        }
        OrderBookResponse orderBookResponse = new OrderBookResponse();
        orderBookResponse.setPrice(bigDecimal.toString());
        return orderBookResponse;
    }

    public void deleteUser(String loginToken) throws BadRequestException {
        Object username = redisService.getRedisStringByKeyValue(loginToken);
        if(username == null){
            logger.error("Delete user info fail with login token invalid or expired");
            throw new BadRequestException(Response.LOGIN_TOKEN_INVALID_OR_EXPIRED.getCode(), Response.LOGIN_TOKEN_INVALID_OR_EXPIRED.getMessage());
        }

        UsersEntity usersData = usersRepository.findByUsername(username.toString());

        if(usersData == null){
            logger.error("Delete user info fail with username not found, username : [{}]", username.toString());
            throw new BadRequestException(Response.USER_NOT_FOUND.getCode(), Response.USER_NOT_FOUND.getMessage());
        }

        Set<OrderEntity> orderEntityList =  orderRepository.findAllByUserId(usersData.getId());

        if(!orderEntityList.isEmpty()){
            for (OrderEntity orderEntity: orderEntityList){
                orderRepository.delete(orderEntity);
            }
            logger.info("Delete order success, username : [{}]", username.toString());
        }
        usersRepository.delete(usersData);
        redisService.deleteKeyRedisString(loginToken);
        logger.info("Delete user success, username : [{}]", username.toString());

    }
}
