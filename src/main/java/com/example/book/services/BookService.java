package com.example.book.services;

import com.example.book.configs.ApplicationConfiguration;
import com.example.book.constants.Response;
import com.example.book.entities.BooksEntity;
import com.example.book.exceptions.CommonException;
import com.example.book.models.BooksDetail;
import com.example.book.models.BooksDetailResponse;
import com.example.book.repositories.BooksRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class BookService {
    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    private BooksRepository booksRepository;

    private RestTemplate restTemplate;

    private ApplicationConfiguration applicationConfiguration;


    @Autowired
    public BookService(BooksRepository booksRepository, ApplicationConfiguration applicationConfiguration,
                       RestTemplate restTemplate) {
        this.booksRepository = booksRepository;
        this.applicationConfiguration = applicationConfiguration;
        this.restTemplate = restTemplate;

    }

    public List<BooksDetailResponse> getBooksInformation() {
        List<BooksEntity> booksEntities = booksRepository.findAllByOrderByIsRecommendedDesc(Sort.by("name"));
        List<BooksDetailResponse> booksDetailList = new ArrayList<>();
        for(BooksEntity booksEntity: booksEntities){
            booksDetailList.add(buildBooksDetail(booksEntity));
        }
        return booksDetailList;
    }

    public List<BooksDetail> getBookList() throws CommonException {
        logger.info("Start to get book list from scb");
        try {
            ResponseEntity<List<BooksDetail>> responseEntity = restTemplate.exchange(applicationConfiguration.getBookListUrl(), HttpMethod.GET,null, new ParameterizedTypeReference<List<BooksDetail>>(){});
            logger.info("Get book list from scb success");
            return responseEntity.getBody();
        } catch (Exception e){
            logger.error("Get book list from scb error, message:[{}]", e.getMessage());
            throw new CommonException(Response.INTERNAL_SERVER_ERROR.getCode(), Response.INTERNAL_SERVER_ERROR.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public List getRecommendedBookList() throws CommonException {
        logger.info("Start to get recommended book list from scb");
        try {
            ResponseEntity<List<BooksDetail>> responseEntity = restTemplate.exchange(applicationConfiguration.getRecommendedBookListUrl(), HttpMethod.GET,null, new ParameterizedTypeReference<List<BooksDetail>>(){});
            logger.info("Get recommended book list from scb success");
            return responseEntity.getBody();
        } catch (Exception e){
            logger.error("Get recommended book list from scb error, message:[{}]", e.getMessage());
            throw new CommonException(Response.INTERNAL_SERVER_ERROR.getCode(), Response.INTERNAL_SERVER_ERROR.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public void saveBookData() throws CommonException {
        List<BooksDetail> booksDetails = getBookList();
        for (BooksDetail booksDetail: booksDetails){
            BooksEntity booksEntity = booksRepository.findById(booksDetail.getId());
            if(booksEntity == null){
                BooksEntity newBookEntity = buildBookEntity(booksDetail);
                booksRepository.save(newBookEntity);
            }
        }
        logger.info("Save book list success");
    }

    public void saveRecommendedBookData() throws CommonException {
        List<BooksDetail> booksDetails = getRecommendedBookList();
        for (BooksDetail booksDetail: booksDetails){
            BooksEntity booksEntity = booksRepository.findById(booksDetail.getId());
            if(booksEntity == null){
                BooksEntity newBookEntity = buildBookEntityFromRecommendedData(booksDetail);
                booksRepository.save(newBookEntity);
            } else {
                booksEntity.setRecommended(true);
                booksRepository.save(booksEntity);
            }
        }
        logger.info("Save recommended book list success");
    }

    private BooksDetailResponse buildBooksDetail(BooksEntity booksEntity){
        BooksDetailResponse booksDetail = new BooksDetailResponse();
        booksDetail.setId(booksEntity.getId());
        booksDetail.setAuthor(booksEntity.getAuthor());
        booksDetail.setName(booksEntity.getName());
        booksDetail.setPrice(booksEntity.getPrice());
        booksDetail.setRecommended(booksEntity.isRecommended());
        return booksDetail;
    }

    private BooksEntity buildBookEntity(BooksDetail booksDetail){
        BooksEntity booksEntity = new BooksEntity();
        booksEntity.setId(booksDetail.getId());
        booksEntity.setAuthor(booksDetail.getAuthorName());
        booksEntity.setName(booksDetail.getBookName());
        booksEntity.setPrice(booksDetail.getPrice());
        booksEntity.setCreatedDate(new Date());
        return booksEntity;
    }

    private BooksEntity buildBookEntityFromRecommendedData(BooksDetail booksDetail){
        BooksEntity booksEntity = new BooksEntity();
        booksEntity.setId(booksDetail.getId());
        booksEntity.setAuthor(booksDetail.getAuthorName());
        booksEntity.setName(booksDetail.getBookName());
        booksEntity.setPrice(booksDetail.getPrice());
        booksEntity.setCreatedDate(new Date());
        booksEntity.setRecommended(true);
        return booksEntity;
    }


}
