package com.example.book.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class RedisService {

    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    public RedisService(StringRedisTemplate stringRedisTemplate) {
        this.stringRedisTemplate = stringRedisTemplate;
    }

    public void deleteKeyRedisString(String key) {
        stringRedisTemplate.delete(key);
    }

    public void putRedisString(String key, String data, long expire) {
        stringRedisTemplate.opsForValue().set(key, data, expire, TimeUnit.SECONDS);
    }

    public Object getRedisStringByKeyValue(String keyValue) {
        return stringRedisTemplate.opsForValue().get(keyValue);
    }

}
