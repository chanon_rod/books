package com.example.book.models;


import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class BooksDetail {

    private Long id;
    private String bookName;
    private String authorName;
    private String price;
    private boolean isRecommended;
}
