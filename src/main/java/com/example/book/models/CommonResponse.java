package com.example.book.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommonResponse<T> {
    private Status status;
    private T data;

    public CommonResponse(Status status, T data) {
        this.status = status;
        this.data = data;
    }

    public CommonResponse(Status status) {
        this.status = status;
    }

    public HttpEntity<CommonResponse> build(HttpStatus status) {
        return new ResponseEntity<>(new CommonResponse(this.status, this.data), status);
    }
}
