package com.example.book.models;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

@Data
public class BooksDetailResponse {

    private Long id;
    private String name;
    private String author;
    private String price;

    @JsonProperty("is_recommended")
    private boolean isRecommended;
}
