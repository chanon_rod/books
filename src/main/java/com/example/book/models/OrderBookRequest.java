package com.example.book.models;

import lombok.Data;

import java.util.List;

@Data
public class OrderBookRequest {
    List<Long> orders;
}
