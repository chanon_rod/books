package com.example.book.models;

import lombok.Data;

@Data
public class UserRequest {

    private String username;
    private String password;
}
