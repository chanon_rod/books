package com.example.book.configs;

import lombok.Data;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class ApplicationConfiguration {

    @Value("${encryptKey}")
    String encryptKey;

    @Value("${bookListUrl}")
    String bookListUrl;

    @Value("${recommendedBookListUrl}")
    String recommendedBookListUrl;

}
