package com.example.book.cronJob;

import com.example.book.exceptions.CommonException;
import com.example.book.services.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class Scheduler {

    private static final Logger logger = LoggerFactory.getLogger(Scheduler.class);

    @Autowired
    BookService bookService;
    @Scheduled(cron = "0 0 0 * * SAT", zone="Asia/Ho_Chi_Minh")
    public void cronJobSch() {
        try {
            bookService.saveBookData();
            bookService.saveRecommendedBookData();
        } catch (CommonException e) {
            logger.error("CronJob save book fail, message:[{}]", e.getMessage());
        }

    }
}
