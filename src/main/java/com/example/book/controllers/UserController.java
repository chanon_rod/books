package com.example.book.controllers;

import com.example.book.constants.Constant;
import com.example.book.constants.Response;
import com.example.book.exceptions.BadRequestException;
import com.example.book.exceptions.CommonException;
import com.example.book.models.*;
import com.example.book.services.UserService;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @ApiResponses
    ( value = {
            @ApiResponse(code = 200, message = "Success", responseHeaders = {
                    @ResponseHeader(name = "Login-Token", response = String.class)
            })
    })
    @PostMapping(value = "/login")
    public ResponseEntity login(@RequestBody UserRequest userRequest) throws BadRequestException, CommonException {
        logger.info("Start to login, username : [{}]", userRequest.getUsername());

        validateLoginRequest(userRequest);
        String loginTokenKey = userService.login(userRequest);
        HttpHeaders headers = new HttpHeaders();
        headers.set(Constant.LOGIN_TOKEN, loginTokenKey);

        logger.info("User login success");
        return new ResponseEntity(headers, HttpStatus.OK);
    }

    @PostMapping(value = "/users")
    public ResponseEntity createUser(@RequestBody CreateUserRequest createUserRequest) throws BadRequestException, CommonException {
        logger.info("Start to create user, username : [{}]", createUserRequest.getUsername());

        validateCreateUserRequest(createUserRequest);
        userService.createUser(createUserRequest);
        logger.info("User login success");
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping(value = "/users")
    public ResponseEntity getUserInformation(@RequestHeader(Constant.LOGIN_TOKEN) String loginToken) throws BadRequestException, CommonException {
        logger.info("Start to get user information");

        UserInformation userInformation = userService.getUserInfo(loginToken);
        logger.info("Get user information success");
        return new ResponseEntity(userInformation, HttpStatus.OK);
    }

    @DeleteMapping(value = "/users")
    public ResponseEntity deleteUser(@RequestHeader(Constant.LOGIN_TOKEN) String loginToken) throws BadRequestException, CommonException {
        logger.info("Start to delete user information");

        userService.deleteUser(loginToken);
        logger.info("Delete user information success");
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping(value = "/users/orders")
    public ResponseEntity orderBook(@RequestHeader(Constant.LOGIN_TOKEN) String loginToken, @RequestBody OrderBookRequest orderBookRequest) throws BadRequestException, CommonException {
        logger.info("Start to order book");

        OrderBookResponse orderBookResponse = userService.orderBook(loginToken, orderBookRequest);
        logger.info("Order book success");
        return new ResponseEntity(orderBookResponse, HttpStatus.OK);
    }


    private void validateLoginRequest(UserRequest userRequest) throws BadRequestException {
        if(userRequest.getUsername() == null || userRequest.getUsername().isEmpty()){
            logger.error("Login fail with username is invalid, username : [{}]", userRequest.getUsername());
            throw new BadRequestException(Response.INVALID_REQUEST.getCode(), String.format(Response.INVALID_REQUEST.getMessage(), Constant.USERNAME));
        }

        if(userRequest.getPassword() == null || userRequest.getPassword().isEmpty()){
            logger.error("Login fail with password is invalid, username : [{}]", userRequest.getUsername());
            throw new BadRequestException(Response.INVALID_REQUEST.getCode(), String.format(Response.INVALID_REQUEST.getMessage(), Constant.PASSWORD));
        }

    }

    private void validateCreateUserRequest(CreateUserRequest userRequest) throws BadRequestException {
        validateLoginRequest(userRequest);
        final String regex = "(0[1-9]|[12][0-9]|3[01])[\\/](0[1-9]|1[012])[\\/](19|20)\\d{2}";
        if (userRequest.getDateOfBirth() == null || !userRequest.getDateOfBirth().matches(regex)) {
            logger.error("Login fail with password is invalid, username : [{}]", userRequest.getUsername());
            throw new BadRequestException(Response.INVALID_REQUEST.getCode(), String.format(Response.INVALID_REQUEST.getMessage(), Constant.DATE_OF_BIRTH));
        }
    }
}
