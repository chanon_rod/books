package com.example.book.controllers;

import com.example.book.constants.Response;
import com.example.book.exceptions.BadRequestException;
import com.example.book.exceptions.CommonException;
import com.example.book.models.CommonResponse;
import com.example.book.models.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class GlobalExceptionControllerAdvice {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionControllerAdvice.class);


    @ExceptionHandler(Exception.class)
    @ResponseBody
    public HttpEntity<CommonResponse> exception(Exception e) {
        logger.error("Internal server error msg: [{}]", e.getMessage());
        return new CommonResponse(new Status(Response.INTERNAL_SERVER_ERROR.getCode(),Response.INTERNAL_SERVER_ERROR.getMessage())).build(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseBody
    public HttpEntity<CommonResponse> badRequestException(BadRequestException e) {
        logger.error("Bad request error msg: [{}]", e.getMessage());
        return new CommonResponse(new Status(e.getCode(),e.getMessage())).build(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CommonException.class)
    @ResponseBody
    public HttpEntity<CommonResponse> commonException(CommonException e) {
        logger.error("Common Exception error msg: [{}]", e.getMessage());
        return new CommonResponse(new Status(e.getCode(),e.getMessage())).build(e.getHttpStatus());
    }


}
