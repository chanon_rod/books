package com.example.book.controllers;

import com.example.book.constants.Constant;
import com.example.book.constants.Response;
import com.example.book.exceptions.BadRequestException;
import com.example.book.exceptions.CommonException;
import com.example.book.models.*;
import com.example.book.services.BookService;
import com.example.book.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.List;

@RestController
public class BookController {

    private static final Logger logger = LoggerFactory.getLogger(BookController.class);

    @Autowired
    private BookService bookService;

    @PostConstruct
    public void initialize() {
        try {
            bookService.saveBookData();
            bookService.saveRecommendedBookData();
        } catch (CommonException e) {
            logger.error("CronJob save book fail, message:[{}]", e.getMessage());
        }
    }


    @GetMapping(value = "/books")
    public ResponseEntity getBookInformation() {
        logger.info("Start to get book information list");
        List<BooksDetailResponse> bookList = bookService.getBooksInformation();
        logger.info("Get book information list success");
        return new ResponseEntity(bookList, HttpStatus.OK);
    }



}
