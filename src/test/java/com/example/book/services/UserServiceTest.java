package com.example.book.services;

import com.example.book.configs.ApplicationConfiguration;
import com.example.book.entities.BooksEntity;
import com.example.book.entities.OrderEntity;
import com.example.book.entities.UsersEntity;
import com.example.book.exceptions.BadRequestException;
import com.example.book.exceptions.CommonException;
import com.example.book.models.*;
import com.example.book.repositories.BooksRepository;
import com.example.book.repositories.OrderRepository;
import com.example.book.repositories.UsersRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class UserServiceTest {
    @InjectMocks
    UserService userService;

    @Mock
    private UsersRepository usersRepository;

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private BooksRepository booksRepository;

    @Mock
    private ApplicationConfiguration applicationConfiguration;

    @Mock
    private RedisService redisService;

    @BeforeEach
    public void init(){
        MockitoAnnotations.openMocks(this);
        Mockito.when(applicationConfiguration.getEncryptKey()).thenReturn("testkey");
    }

    @Test
    public void createUserSuccess() throws BadRequestException, CommonException {

        Mockito.when(usersRepository.findByUsername("test")).thenReturn(null);

        CreateUserRequest createUserRequest = new CreateUserRequest();
        createUserRequest.setUsername("test");
        createUserRequest.setPassword("password");
        createUserRequest.setName("name");
        createUserRequest.setSurname("surname");
        createUserRequest.setDateOfBirth("15/01/1985");
        userService.createUser(createUserRequest);
        Mockito.verify(usersRepository, Mockito.times(1)).save(Mockito.any(UsersEntity.class));
    }

    @Test
    public void createUserFailWithEncryptError(){
        Mockito.when(applicationConfiguration.getEncryptKey()).thenReturn(null);
        Mockito.when(usersRepository.findByUsername("test")).thenReturn(null);

        CreateUserRequest createUserRequest = new CreateUserRequest();
        createUserRequest.setUsername("test");
        createUserRequest.setPassword("password");
        createUserRequest.setName("name");
        createUserRequest.setSurname("surname");
        createUserRequest.setDateOfBirth("15/01/1985");
        Assertions.assertThrows(CommonException.class, () -> {
            userService.createUser(createUserRequest);
        });
        Mockito.verify(usersRepository, Mockito.times(0)).save(Mockito.any(UsersEntity.class));
    }

    @Test
    public void createUserFailWithExistingUser() {

        UsersEntity usersEntity = new UsersEntity();
        usersEntity.setUsername("test");
        usersEntity.setPassword("38ygVCgw/6F0vfqTUeoOd/ecGIRHLDtkz1T7tvpA1BIfnk1RIBicN+F7sg1TctTj0visFQ==");
        usersEntity.setName("name");
        usersEntity.setSurname("surname");
        usersEntity.setDateOfBirth("15/01/1985");
        usersEntity.setCreatedDate(new Date());
        usersEntity.setId(1L);
        Mockito.when(usersRepository.findByUsername("test")).thenReturn(usersEntity);

        CreateUserRequest createUserRequest = new CreateUserRequest();
        createUserRequest.setUsername("test");
        createUserRequest.setPassword("password");
        createUserRequest.setName("name");
        createUserRequest.setSurname("surname");
        createUserRequest.setDateOfBirth("15/01/1985");

        Assertions.assertThrows(BadRequestException.class, () -> {
            userService.createUser(createUserRequest);
        });
        Mockito.verify(usersRepository, Mockito.times(0)).save(Mockito.any(UsersEntity.class));
    }

    @Test
    public void loginSuccess() throws BadRequestException, CommonException {

        UsersEntity usersEntity = new UsersEntity();
        usersEntity.setUsername("test");
        usersEntity.setPassword("38ygVCgw/6F0vfqTUeoOd/ecGIRHLDtkz1T7tvpA1BIfnk1RIBicN+F7sg1TctTj0visFQ==");
        usersEntity.setName("name");
        usersEntity.setSurname("surname");
        usersEntity.setDateOfBirth("15/01/1985");
        usersEntity.setCreatedDate(new Date());
        usersEntity.setId(1L);
        Mockito.when(usersRepository.findByUsername("test")).thenReturn(usersEntity);

        UserRequest createUserRequest = new UserRequest();
        createUserRequest.setUsername("test");
        createUserRequest.setPassword("password");
        userService.login(createUserRequest);
        Mockito.verify(redisService, Mockito.times(1)).putRedisString(Mockito.anyString(),Mockito.anyString(),Mockito.anyLong());
    }

    @Test
    public void loginFailWithWrongPassword(){

        UsersEntity usersEntity = new UsersEntity();
        usersEntity.setUsername("test");
        usersEntity.setPassword("meZmV5fsvwFiq011a+JeuoC345AkzqVPVrODyUNiQOGUHzoq+ZaR5hDza5jwwziQm2iaoA==");
        usersEntity.setName("name");
        usersEntity.setSurname("surname");
        usersEntity.setDateOfBirth("15/01/1985");
        usersEntity.setCreatedDate(new Date());
        usersEntity.setId(1L);
        Mockito.when(usersRepository.findByUsername("test")).thenReturn(usersEntity);

        UserRequest createUserRequest = new UserRequest();
        createUserRequest.setUsername("test");
        createUserRequest.setPassword("password");
        Assertions.assertThrows(BadRequestException.class, () -> {
            userService.login(createUserRequest);
        });
        Mockito.verify(redisService, Mockito.times(0)).putRedisString(Mockito.anyString(),Mockito.anyString(),Mockito.anyLong());
    }

    @Test
    public void loginFailWithPasswordCannotDecrypt(){

        UsersEntity usersEntity = new UsersEntity();
        usersEntity.setUsername("test");
        usersEntity.setPassword("rewr");
        usersEntity.setName("name");
        usersEntity.setSurname("surname");
        usersEntity.setDateOfBirth("15/01/1985");
        usersEntity.setCreatedDate(new Date());
        usersEntity.setId(1L);
        Mockito.when(usersRepository.findByUsername("test")).thenReturn(usersEntity);

        UserRequest createUserRequest = new UserRequest();
        createUserRequest.setUsername("test");
        createUserRequest.setPassword("password");
        Assertions.assertThrows(CommonException.class, () -> {
            userService.login(createUserRequest);
        });
        Mockito.verify(redisService, Mockito.times(0)).putRedisString(Mockito.anyString(),Mockito.anyString(),Mockito.anyLong());
    }

    @Test
    public void loginFailWithUserNotFound(){
        Mockito.when(usersRepository.findByUsername("test")).thenReturn(null);

        UserRequest createUserRequest = new UserRequest();
        createUserRequest.setUsername("test");
        createUserRequest.setPassword("password");
        Assertions.assertThrows(BadRequestException.class, () -> {
            userService.login(createUserRequest);
        });
        Mockito.verify(redisService, Mockito.times(0)).putRedisString(Mockito.anyString(),Mockito.anyString(),Mockito.anyLong());
    }

    @Test
    public void getUserInfoSuccess() throws BadRequestException {

        UsersEntity usersEntity = new UsersEntity();
        usersEntity.setUsername("test");
        usersEntity.setPassword("38ygVCgw/6F0vfqTUeoOd/ecGIRHLDtkz1T7tvpA1BIfnk1RIBicN+F7sg1TctTj0visFQ==");
        usersEntity.setName("name");
        usersEntity.setSurname("surname");
        usersEntity.setDateOfBirth("15/01/1985");
        usersEntity.setCreatedDate(new Date());
        usersEntity.setId(1L);
        Mockito.when(usersRepository.findByUsername("test")).thenReturn(usersEntity);

        Mockito.when(redisService.getRedisStringByKeyValue("login-token")).thenReturn("test");

        Set<OrderEntity> orderEntityList = new HashSet<>();
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setCreatedDate(new Date());
        orderEntity.setUserId(1L);
        orderEntity.setBookId(1L);
        orderEntity.setId(1L);
        orderEntityList.add(orderEntity);

        Mockito.when(orderRepository.findAllByUserId(1L)).thenReturn(orderEntityList);
        UserInformation userInformation = userService.getUserInfo("login-token");
        assertEquals("name",userInformation.getName());
        assertEquals("surname",userInformation.getSurname());
        assertEquals("15/01/1985",userInformation.getDateOfBirth());
        assertEquals(1,userInformation.getBooks().get(0));
    }

    @Test
    public void getUserInfoFailWithInvalidLoginToken() throws BadRequestException {

        Mockito.when(redisService.getRedisStringByKeyValue("login-token")).thenReturn(null);

        Set<OrderEntity> orderEntityList = new HashSet<>();
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setCreatedDate(new Date());
        orderEntity.setUserId(1L);
        orderEntity.setBookId(1L);
        orderEntity.setId(1L);
        orderEntityList.add(orderEntity);

        Assertions.assertThrows(BadRequestException.class, () -> {
            userService.getUserInfo("login-token");
        });
        Mockito.verify(redisService, Mockito.times(1)).getRedisStringByKeyValue("login-token");

    }

    @Test
    public void getUserInfoFailWithUserNotFound() throws BadRequestException {

        Mockito.when(usersRepository.findByUsername("test")).thenReturn(null);
        Mockito.when(redisService.getRedisStringByKeyValue("login-token")).thenReturn("test");

        Assertions.assertThrows(BadRequestException.class, () -> {
            userService.getUserInfo("login-token");
        });
        Mockito.verify(redisService, Mockito.times(1)).getRedisStringByKeyValue("login-token");
        Mockito.verify(usersRepository, Mockito.times(1)).findByUsername("test");
    }

    @Test
    public void orderBookSuccess() throws BadRequestException {

        UsersEntity usersEntity = new UsersEntity();
        usersEntity.setUsername("test");
        usersEntity.setPassword("38ygVCgw/6F0vfqTUeoOd/ecGIRHLDtkz1T7tvpA1BIfnk1RIBicN+F7sg1TctTj0visFQ==");
        usersEntity.setName("name");
        usersEntity.setSurname("surname");
        usersEntity.setDateOfBirth("15/01/1985");
        usersEntity.setCreatedDate(new Date());
        usersEntity.setId(1L);
        Mockito.when(usersRepository.findByUsername("test")).thenReturn(usersEntity);

        Mockito.when(redisService.getRedisStringByKeyValue("login-token")).thenReturn("test");

        Set<OrderEntity> orderEntityList = new HashSet<>();
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setCreatedDate(new Date());
        orderEntity.setUserId(1L);
        orderEntity.setBookId(1L);
        orderEntity.setId(1L);
        orderEntityList.add(orderEntity);

        Mockito.when(orderRepository.findAllByUserId(1L)).thenReturn(orderEntityList);

        BooksEntity booksEntity = new BooksEntity();
        booksEntity.setId(1L);
        booksEntity.setName("name");
        booksEntity.setAuthor("author");
        booksEntity.setRecommended(true);
        booksEntity.setPrice("12.32");
        booksEntity.setCreatedDate(new Date());

        Mockito.when(booksRepository.findById(1L)).thenReturn(booksEntity);

        BooksEntity booksEntity2 = new BooksEntity();
        booksEntity2.setId(2L);
        booksEntity2.setName("name2");
        booksEntity2.setAuthor("author");
        booksEntity2.setRecommended(true);
        booksEntity2.setPrice("12.68");
        booksEntity2.setCreatedDate(new Date());

        Mockito.when(booksRepository.findById(2L)).thenReturn(booksEntity2);

        List<Long> orderList = new ArrayList<>();
        orderList.add(1L);
        orderList.add(2L);
        OrderBookRequest orderBookRequest = new OrderBookRequest();
        orderBookRequest.setOrders(orderList);
        OrderBookResponse orderBookResponse = userService.orderBook("login-token",orderBookRequest);
        assertEquals("25.00",orderBookResponse.getPrice());

    }

    @Test
    public void orderBookFailWithInvalidLoginToken() {

        Mockito.when(redisService.getRedisStringByKeyValue("login-token")).thenReturn(null);

        Set<OrderEntity> orderEntityList = new HashSet<>();
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setCreatedDate(new Date());
        orderEntity.setUserId(1L);
        orderEntity.setBookId(1L);
        orderEntity.setId(1L);
        orderEntityList.add(orderEntity);

        Assertions.assertThrows(BadRequestException.class, () -> {
            List<Long> orderList = new ArrayList<>();
            orderList.add(1L);
            orderList.add(2L);
            OrderBookRequest orderBookRequest = new OrderBookRequest();
            orderBookRequest.setOrders(orderList);
            userService.orderBook("login-token",orderBookRequest);
        });
        Mockito.verify(redisService, Mockito.times(1)).getRedisStringByKeyValue("login-token");

    }

    @Test
    public void orderBookFailWithUserNotFound() {

        Mockito.when(usersRepository.findByUsername("test")).thenReturn(null);
        Mockito.when(redisService.getRedisStringByKeyValue("login-token")).thenReturn("test");

        Assertions.assertThrows(BadRequestException.class, () -> {
            List<Long> orderList = new ArrayList<>();
            orderList.add(1L);
            orderList.add(2L);
            OrderBookRequest orderBookRequest = new OrderBookRequest();
            orderBookRequest.setOrders(orderList);
            userService.orderBook("login-token",orderBookRequest);
        });
        Mockito.verify(redisService, Mockito.times(1)).getRedisStringByKeyValue("login-token");
        Mockito.verify(usersRepository, Mockito.times(1)).findByUsername("test");
    }

    @Test
    public void orderBookFailWithOrderNotFound() {

        UsersEntity usersEntity = new UsersEntity();
        usersEntity.setUsername("test");
        usersEntity.setPassword("38ygVCgw/6F0vfqTUeoOd/ecGIRHLDtkz1T7tvpA1BIfnk1RIBicN+F7sg1TctTj0visFQ==");
        usersEntity.setName("name");
        usersEntity.setSurname("surname");
        usersEntity.setDateOfBirth("15/01/1985");
        usersEntity.setCreatedDate(new Date());
        usersEntity.setId(1L);
        Mockito.when(usersRepository.findByUsername("test")).thenReturn(usersEntity);

        Mockito.when(redisService.getRedisStringByKeyValue("login-token")).thenReturn("test");

        Set<OrderEntity> orderEntityList = new HashSet<>();
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setCreatedDate(new Date());
        orderEntity.setUserId(1L);
        orderEntity.setBookId(1L);
        orderEntity.setId(1L);
        orderEntityList.add(orderEntity);

        Mockito.when(orderRepository.findAllByUserId(1L)).thenReturn(orderEntityList);

        Mockito.when(booksRepository.findById(1L)).thenReturn(null);

        Assertions.assertThrows(BadRequestException.class, () -> {
            List<Long> orderList = new ArrayList<>();
            orderList.add(1L);
            orderList.add(2L);
            OrderBookRequest orderBookRequest = new OrderBookRequest();
            orderBookRequest.setOrders(orderList);
            userService.orderBook("login-token",orderBookRequest);
        });
        Mockito.verify(redisService, Mockito.times(1)).getRedisStringByKeyValue("login-token");
        Mockito.verify(usersRepository, Mockito.times(1)).findByUsername("test");
        Mockito.verify(booksRepository, Mockito.times(1)).findById(1L);
    }

    @Test
    public void deleteUserSuccess() throws BadRequestException {

        UsersEntity usersEntity = new UsersEntity();
        usersEntity.setUsername("test");
        usersEntity.setPassword("38ygVCgw/6F0vfqTUeoOd/ecGIRHLDtkz1T7tvpA1BIfnk1RIBicN+F7sg1TctTj0visFQ==");
        usersEntity.setName("name");
        usersEntity.setSurname("surname");
        usersEntity.setDateOfBirth("15/01/1985");
        usersEntity.setCreatedDate(new Date());
        usersEntity.setId(1L);
        Mockito.when(usersRepository.findByUsername("test")).thenReturn(usersEntity);

        Mockito.when(redisService.getRedisStringByKeyValue("login-token")).thenReturn("test");

        Set<OrderEntity> orderEntityList = new HashSet<>();
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setCreatedDate(new Date());
        orderEntity.setUserId(1L);
        orderEntity.setBookId(1L);
        orderEntity.setId(1L);
        orderEntityList.add(orderEntity);

        Mockito.when(orderRepository.findAllByUserId(1L)).thenReturn(orderEntityList);


        userService.deleteUser("login-token");
        Mockito.verify(redisService, Mockito.times(1)).getRedisStringByKeyValue("login-token");
        Mockito.verify(usersRepository, Mockito.times(1)).findByUsername("test");
        Mockito.verify(orderRepository, Mockito.times(1)).findAllByUserId(1L);
        Mockito.verify(redisService, Mockito.times(1)).deleteKeyRedisString("login-token");
        Mockito.verify(usersRepository, Mockito.times(1)).delete(Mockito.any());
        Mockito.verify(orderRepository, Mockito.times(1)).delete(Mockito.any());

    }

    @Test
    public void deleteUserFailWithInvalidLoginToken() {

        Mockito.when(redisService.getRedisStringByKeyValue("login-token")).thenReturn(null);

        Set<OrderEntity> orderEntityList = new HashSet<>();
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setCreatedDate(new Date());
        orderEntity.setUserId(1L);
        orderEntity.setBookId(1L);
        orderEntity.setId(1L);
        orderEntityList.add(orderEntity);

        Assertions.assertThrows(BadRequestException.class, () -> {
            userService.deleteUser("login-token");
        });
        Mockito.verify(redisService, Mockito.times(1)).getRedisStringByKeyValue("login-token");

    }

    @Test
    public void deleteUserFailWithUserNotFound() {

        Mockito.when(usersRepository.findByUsername("test")).thenReturn(null);
        Mockito.when(redisService.getRedisStringByKeyValue("login-token")).thenReturn("test");

        Assertions.assertThrows(BadRequestException.class, () -> {
            userService.deleteUser("login-token");
        });
        Mockito.verify(redisService, Mockito.times(1)).getRedisStringByKeyValue("login-token");
        Mockito.verify(usersRepository, Mockito.times(1)).findByUsername("test");
    }


}