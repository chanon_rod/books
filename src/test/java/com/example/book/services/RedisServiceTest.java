package com.example.book.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

class RedisServiceTest {

    @InjectMocks
    RedisService redisService;

    @Mock
    private StringRedisTemplate stringRedisTemplate;

    @Mock
    ValueOperations valueOperations;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void deleteKeyRedisStringSuccess() {
        Mockito.when(stringRedisTemplate.delete("delete")).thenReturn(true);
        redisService.deleteKeyRedisString("delete");
        Mockito.verify(stringRedisTemplate, Mockito.times(1)).delete("delete");
    }

    @Test
    public void putRedisStringSuccess() {
        Mockito.when(stringRedisTemplate.opsForValue()).thenReturn(valueOperations);
        redisService.putRedisString("key", "value", 900);
        Mockito.verify(stringRedisTemplate, Mockito.times(1)).opsForValue();

    }

    @Test
    public void getRedisStringByKeyValueSuccess() {
        Mockito.when(stringRedisTemplate.opsForValue()).thenReturn(valueOperations);
        redisService.getRedisStringByKeyValue("key");
        Mockito.verify(stringRedisTemplate, Mockito.times(1)).opsForValue();

    }
}