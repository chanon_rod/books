package com.example.book.services;

import com.example.book.configs.ApplicationConfiguration;
import com.example.book.entities.BooksEntity;
import com.example.book.exceptions.CommonException;
import com.example.book.models.BooksDetail;
import com.example.book.models.BooksDetailResponse;
import com.example.book.repositories.BooksRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BookServiceTest {

    @InjectMocks
    BookService bookService;

    @Mock
    private BooksRepository booksRepository;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private ApplicationConfiguration applicationConfiguration;

    @BeforeEach
    public void init(){
        MockitoAnnotations.openMocks(this);
        Mockito.when(applicationConfiguration.getBookListUrl()).thenReturn("https://test.com/books");
        Mockito.when(applicationConfiguration.getRecommendedBookListUrl()).thenReturn("https://test.com/recommendedbooks");
    }

    @Test
    public void getBooksInformationSuccess(){

        List<BooksEntity> booksEntityList = new ArrayList<>();
        BooksEntity booksEntity = new BooksEntity();
        booksEntity.setId(1L);
        booksEntity.setName("Before");
        booksEntity.setAuthor("Lisa");
        booksEntity.setPrice("127.00");
        booksEntity.setCreatedDate(new Date());
        booksEntity.setRecommended(true);
        booksEntityList.add(booksEntity);
        Mockito.when(booksRepository.findAllByOrderByIsRecommendedDesc(Mockito.any())).thenReturn(booksEntityList);

        List<BooksDetailResponse> booksDetailResponses = bookService.getBooksInformation();
        assertEquals(1,booksDetailResponses.get(0).getId());
        assertEquals("Before",booksDetailResponses.get(0).getName());
        assertEquals("Lisa",booksDetailResponses.get(0).getAuthor());
        assertEquals("127.00",booksDetailResponses.get(0).getPrice());
        assertEquals(true,booksDetailResponses.get(0).isRecommended());
    }

    @Test
    public void saveBookDataSuccess() throws CommonException {
        List<BooksDetail> booksDetailList = new ArrayList<>();
        BooksDetail booksDetail = new BooksDetail();
        booksDetail.setBookName("Before");
        booksDetail.setAuthorName("Lisa");
        booksDetail.setId(1L);
        booksDetail.setPrice("127.00");
        booksDetailList.add(booksDetail);
        ResponseEntity<List<BooksDetail>> booksEntityList = new ResponseEntity<>(booksDetailList, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(Mockito.eq("https://test.com/books"), Mockito.any(HttpMethod.class), Mockito.eq(null), Mockito.eq(new ParameterizedTypeReference<List<BooksDetail>>(){}))).thenReturn(booksEntityList);

        Mockito.when(booksRepository.findById(Mockito.any(Long.class))).thenReturn(null);

        bookService.saveBookData();
        Mockito.verify(booksRepository, Mockito.times(1)).save(Mockito.any(BooksEntity.class));

    }

    @Test
    public void saveBookDataSuccessWithExistBookData() throws CommonException {
        List<BooksDetail> booksDetailList = new ArrayList<>();
        BooksDetail booksDetail = new BooksDetail();
        booksDetail.setBookName("Before");
        booksDetail.setAuthorName("Lisa");
        booksDetail.setId(1L);
        booksDetail.setPrice("127.00");
        booksDetailList.add(booksDetail);
        ResponseEntity<List<BooksDetail>> booksEntityList = new ResponseEntity<>(booksDetailList, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(Mockito.eq("https://test.com/books"), Mockito.any(HttpMethod.class), Mockito.eq(null), Mockito.eq(new ParameterizedTypeReference<List<BooksDetail>>(){}))).thenReturn(booksEntityList);

        BooksEntity booksEntity = new BooksEntity();
        booksEntity.setId(1L);
        booksEntity.setName("Before");
        booksEntity.setAuthor("Lisa");
        booksEntity.setPrice("127.00");
        booksEntity.setCreatedDate(new Date());
        booksEntity.setRecommended(true);
        Mockito.when(booksRepository.findById(Mockito.any(Long.class))).thenReturn(booksEntity);

        bookService.saveBookData();
        Mockito.verify(booksRepository, Mockito.times(0)).save(Mockito.any(BooksEntity.class));

    }

    @Test
    public void saveBookDataFailWithCallApiGetBookListError() throws CommonException {
        Mockito.when(restTemplate.exchange(Mockito.eq("https://test.com/books"), Mockito.any(HttpMethod.class), Mockito.eq(null), Mockito.eq(new ParameterizedTypeReference<List<BooksDetail>>(){}))).thenThrow(HttpClientErrorException.class);
        Assertions.assertThrows(CommonException.class, () -> {
            bookService.saveBookData();
        });
    }

    @Test
    public void saveRecommendedBookDataSuccess() throws CommonException {
        List<BooksDetail> booksDetailList = new ArrayList<>();
        BooksDetail booksDetail = new BooksDetail();
        booksDetail.setBookName("Before");
        booksDetail.setAuthorName("Lisa");
        booksDetail.setId(1L);
        booksDetail.setPrice("127.00");
        booksDetailList.add(booksDetail);
        ResponseEntity<List<BooksDetail>> booksEntityList = new ResponseEntity<>(booksDetailList, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(Mockito.eq("https://test.com/recommendedbooks"), Mockito.any(HttpMethod.class), Mockito.eq(null), Mockito.eq(new ParameterizedTypeReference<List<BooksDetail>>(){}))).thenReturn(booksEntityList);

        Mockito.when(booksRepository.findById(Mockito.any(Long.class))).thenReturn(null);

        bookService.saveRecommendedBookData();
        Mockito.verify(booksRepository, Mockito.times(1)).save(Mockito.any(BooksEntity.class));

    }

    @Test
    public void saveRecommendedBookDataSuccessWithExistBookData() throws CommonException {
        List<BooksDetail> booksDetailList = new ArrayList<>();
        BooksDetail booksDetail = new BooksDetail();
        booksDetail.setBookName("Before");
        booksDetail.setAuthorName("Lisa");
        booksDetail.setId(1L);
        booksDetail.setPrice("127.00");
        booksDetailList.add(booksDetail);
        ResponseEntity<List<BooksDetail>> booksEntityList = new ResponseEntity<>(booksDetailList, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(Mockito.eq("https://test.com/recommendedbooks"), Mockito.any(HttpMethod.class), Mockito.eq(null), Mockito.eq(new ParameterizedTypeReference<List<BooksDetail>>(){}))).thenReturn(booksEntityList);

        BooksEntity booksEntity = new BooksEntity();
        booksEntity.setId(1L);
        booksEntity.setName("Before");
        booksEntity.setAuthor("Lisa");
        booksEntity.setPrice("127.00");
        booksEntity.setCreatedDate(new Date());
        booksEntity.setRecommended(true);
        Mockito.when(booksRepository.findById(Mockito.any(Long.class))).thenReturn(booksEntity);

        bookService.saveRecommendedBookData();
        Mockito.verify(booksRepository, Mockito.times(1)).save(Mockito.any(BooksEntity.class));

    }

    @Test
    public void saveRecommendedBookDataFailWithCallApiGetBookListError() throws CommonException {
        Mockito.when(restTemplate.exchange(Mockito.eq("https://test.com/recommendedbooks"), Mockito.any(HttpMethod.class), Mockito.eq(null), Mockito.eq(new ParameterizedTypeReference<List<BooksDetail>>(){}))).thenThrow(HttpClientErrorException.class);
        Assertions.assertThrows(CommonException.class, () -> {
            bookService.saveRecommendedBookData();
        });
    }
}