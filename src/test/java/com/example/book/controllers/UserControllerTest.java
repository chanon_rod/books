package com.example.book.controllers;

import com.example.book.constants.Constant;
import com.example.book.constants.Response;
import com.example.book.exceptions.CommonException;
import com.example.book.models.*;
import com.example.book.services.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

class UserControllerTest {
    @InjectMocks
    UserController userController;

    @Mock
    UserService userService;

    MockMvc mockMvc;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(userController)
                .setControllerAdvice(new GlobalExceptionControllerAdvice())
                .build();
    }

    @Test
    public void loginSuccess() throws Exception {
        UserRequest userRequest = new UserRequest();
        userRequest.setUsername("name");
        userRequest.setPassword("password");
        ObjectMapper objectMapper = new ObjectMapper();
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(userRequest));

        Mockito.when(userService.login(Mockito.any(UserRequest.class))).thenReturn("login-token");

        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void loginFailWithUsernameIsNull() throws Exception {
        UserRequest userRequest = new UserRequest();
        userRequest.setUsername(null);
        userRequest.setPassword("password");
        ObjectMapper objectMapper = new ObjectMapper();
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(userRequest));

        Mockito.when(userService.login(Mockito.any(UserRequest.class))).thenReturn("login-token");

        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status.code").value(Response.INVALID_REQUEST.getCode()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status.message").value(String.format(Response.INVALID_REQUEST.getMessage(),"username")));
    }

    @Test
    public void loginFailWithUsernameIsEmpty() throws Exception {
        UserRequest userRequest = new UserRequest();
        userRequest.setUsername("");
        userRequest.setPassword("password");
        ObjectMapper objectMapper = new ObjectMapper();
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(userRequest));

        Mockito.when(userService.login(Mockito.any(UserRequest.class))).thenReturn("login-token");

        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status.code").value(Response.INVALID_REQUEST.getCode()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status.message").value(String.format(Response.INVALID_REQUEST.getMessage(),"username")));
    }

    @Test
    public void loginFailWithPasswordIsNull() throws Exception {
        UserRequest userRequest = new UserRequest();
        userRequest.setUsername("username");
        userRequest.setPassword(null);
        ObjectMapper objectMapper = new ObjectMapper();
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(userRequest));

        Mockito.when(userService.login(Mockito.any(UserRequest.class))).thenReturn("login-token");

        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status.code").value(Response.INVALID_REQUEST.getCode()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status.message").value(String.format(Response.INVALID_REQUEST.getMessage(),"password")));
    }

    @Test
    public void loginFailWithPasswordIsEmpty() throws Exception {
        UserRequest userRequest = new UserRequest();
        userRequest.setUsername("username");
        userRequest.setPassword("");
        ObjectMapper objectMapper = new ObjectMapper();
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(userRequest));

        Mockito.when(userService.login(Mockito.any(UserRequest.class))).thenReturn("login-token");

        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status.code").value(Response.INVALID_REQUEST.getCode()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status.message").value(String.format(Response.INVALID_REQUEST.getMessage(),"password")));
    }

    @Test
    public void createUserSuccess() throws Exception {
        CreateUserRequest userRequest = new CreateUserRequest();
        userRequest.setUsername("name");
        userRequest.setPassword("password");
        userRequest.setDateOfBirth("11/11/2011");
        userRequest.setName("test");
        userRequest.setSurname("surname");
        ObjectMapper objectMapper = new ObjectMapper();
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(userRequest));

        Mockito.doNothing().when(userService).createUser(Mockito.any(CreateUserRequest.class));

        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createUserFailWithUsernameIsNull() throws Exception {
        CreateUserRequest userRequest = new CreateUserRequest();
        userRequest.setUsername(null);
        userRequest.setPassword("password");
        userRequest.setDateOfBirth("11/11/2011");
        userRequest.setName("test");
        userRequest.setSurname("surname");
        ObjectMapper objectMapper = new ObjectMapper();
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(userRequest));

        Mockito.doNothing().when(userService).createUser(Mockito.any(CreateUserRequest.class));

        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status.code").value(Response.INVALID_REQUEST.getCode()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status.message").value(String.format(Response.INVALID_REQUEST.getMessage(),"username")));
    }

    @Test
    public void createUserFailWithUsernameIsEmpty() throws Exception {
        CreateUserRequest userRequest = new CreateUserRequest();
        userRequest.setUsername("");
        userRequest.setPassword("password");
        userRequest.setDateOfBirth("11/11/2011");
        userRequest.setName("test");
        userRequest.setSurname("surname");
        ObjectMapper objectMapper = new ObjectMapper();
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(userRequest));

        Mockito.doNothing().when(userService).createUser(Mockito.any(CreateUserRequest.class));

        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status.code").value(Response.INVALID_REQUEST.getCode()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status.message").value(String.format(Response.INVALID_REQUEST.getMessage(),"username")));
    }

    @Test
    public void createUserFailWithPasswordIsNull() throws Exception {
        CreateUserRequest userRequest = new CreateUserRequest();
        userRequest.setUsername("name");
        userRequest.setPassword(null);
        userRequest.setDateOfBirth("11/11/2011");
        userRequest.setName("test");
        userRequest.setSurname("surname");
        ObjectMapper objectMapper = new ObjectMapper();
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(userRequest));

        Mockito.doNothing().when(userService).createUser(Mockito.any(CreateUserRequest.class));

        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status.code").value(Response.INVALID_REQUEST.getCode()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status.message").value(String.format(Response.INVALID_REQUEST.getMessage(),"password")));
    }

    @Test
    public void createUserFailWithPasswordIsEmpty() throws Exception {
        CreateUserRequest userRequest = new CreateUserRequest();
        userRequest.setUsername("name");
        userRequest.setPassword("");
        userRequest.setDateOfBirth("11/11/2011");
        userRequest.setName("test");
        userRequest.setSurname("surname");
        ObjectMapper objectMapper = new ObjectMapper();
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(userRequest));

        Mockito.doNothing().when(userService).createUser(Mockito.any(CreateUserRequest.class));

        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status.code").value(Response.INVALID_REQUEST.getCode()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status.message").value(String.format(Response.INVALID_REQUEST.getMessage(),"password")));
    }

    @Test
    public void createUserFailWithDateOfBirthIsNull() throws Exception {
        CreateUserRequest userRequest = new CreateUserRequest();
        userRequest.setUsername("name");
        userRequest.setPassword("password");
        userRequest.setDateOfBirth(null);
        userRequest.setName("test");
        userRequest.setSurname("surname");
        ObjectMapper objectMapper = new ObjectMapper();
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(userRequest));

        Mockito.doNothing().when(userService).createUser(Mockito.any(CreateUserRequest.class));

        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status.code").value(Response.INVALID_REQUEST.getCode()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status.message").value(String.format(Response.INVALID_REQUEST.getMessage(),"date_of_birth")));
    }

    @Test
    public void createUserFailWithDateOfBirthIsInvalid() throws Exception {
        CreateUserRequest userRequest = new CreateUserRequest();
        userRequest.setUsername("name");
        userRequest.setPassword("password");
        userRequest.setDateOfBirth("1232");
        userRequest.setName("test");
        userRequest.setSurname("surname");
        ObjectMapper objectMapper = new ObjectMapper();
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(userRequest));

        Mockito.doNothing().when(userService).createUser(Mockito.any(CreateUserRequest.class));

        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status.code").value(Response.INVALID_REQUEST.getCode()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status.message").value(String.format(Response.INVALID_REQUEST.getMessage(),"date_of_birth")));
    }

    @Test
    public void createUserFailWithThrowCommonException() throws Exception {
        CreateUserRequest userRequest = new CreateUserRequest();
        userRequest.setUsername("name");
        userRequest.setPassword("password");
        userRequest.setDateOfBirth("11/11/2011");
        userRequest.setName("test");
        userRequest.setSurname("surname");
        ObjectMapper objectMapper = new ObjectMapper();
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(userRequest));
        CommonException commonException =new CommonException(Response.INTERNAL_SERVER_ERROR.getCode(), Response.INTERNAL_SERVER_ERROR.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        Mockito.doThrow(commonException).when(userService).createUser(Mockito.any(CreateUserRequest.class));

        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isInternalServerError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status.code").value(Response.INTERNAL_SERVER_ERROR.getCode()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status.message").value(String.format(Response.INTERNAL_SERVER_ERROR.getMessage(),"username")));
    }

    @Test
    public void createUserFailWithNoBody() throws Exception {
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/users")
                .contentType(MediaType.APPLICATION_JSON);
        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isInternalServerError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status.code").value(Response.INTERNAL_SERVER_ERROR.getCode()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status.message").value(String.format(Response.INTERNAL_SERVER_ERROR.getMessage(),"username")));
    }

    @Test
    public void deleteUserSuccess() throws Exception {
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.delete("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .header(Constant.LOGIN_TOKEN,"login-token");

        Mockito.doNothing().when(userService).deleteUser("login-token");

        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getUserInformationSuccess() throws Exception {
        UserInformation userInformation = new UserInformation();
        userInformation.setDateOfBirth("11/11/2011");
        userInformation.setName("test");
        userInformation.setSurname("surname");
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .header(Constant.LOGIN_TOKEN,"login-token");

        Mockito.when(userService.getUserInfo("login-token")).thenReturn(userInformation);

        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("test"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.surname").value("surname"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.date_of_birth").value("11/11/2011"));
    }

    @Test
    public void orderBookSuccess() throws Exception {

        List<Long> orderList = new ArrayList<>();
        orderList.add(1L);
        orderList.add(2L);
        OrderBookRequest orderBookRequest = new OrderBookRequest();
        orderBookRequest.setOrders(orderList);
        ObjectMapper objectMapper = new ObjectMapper();
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/users/orders")
                .contentType(MediaType.APPLICATION_JSON)
                .header(Constant.LOGIN_TOKEN,"login-token")
                .content(objectMapper.writeValueAsBytes(orderBookRequest));

        OrderBookResponse orderBookResponse = new OrderBookResponse();
        orderBookResponse.setPrice("20.00");
        Mockito.when(userService.orderBook(Mockito.eq("login-token"), Mockito.any(OrderBookRequest.class))).thenReturn(orderBookResponse);

        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.price").value("20.00"));

    }
}