package com.example.book.controllers;

import com.example.book.constants.Constant;
import com.example.book.models.BooksDetailResponse;
import com.example.book.models.UserInformation;
import com.example.book.services.BookService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BookControllerTest {

    @InjectMocks
    BookController bookController;

    @Mock
    BookService bookService;

    MockMvc mockMvc;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(bookController)
                .setControllerAdvice(new GlobalExceptionControllerAdvice())
                .build();
    }

    @Test
    public void getBookInformationSuccess() throws Exception {
        BooksDetailResponse booksDetailResponse = new BooksDetailResponse();
        booksDetailResponse.setName("name");
        booksDetailResponse.setAuthor("author");
        booksDetailResponse.setId(1L);
        booksDetailResponse.setPrice("12.00");
        booksDetailResponse.setRecommended(true);
        List<BooksDetailResponse> booksDetailResponseList = new ArrayList<>();
        booksDetailResponseList.add(booksDetailResponse);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/books");

        Mockito.when(bookService.getBooksInformation()).thenReturn(booksDetailResponseList);

        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("name"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].author").value("author"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].price").value("12.00"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].is_recommended").value(true));
    }

}